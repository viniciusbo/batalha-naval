var Grid = function(rows, cols) {
	this.rows = rows;
	this.cols = cols;
	this.grid = [[]];

	// Start clear grid
	this.clearGrid();
};

Grid.prototype.clearGrid = function() {
	for (var row = 0; row < this.rows; row++) {
		this.grid[row] = [];

		for (var col = 0; col < this.cols; col++) {
			this.grid[row][col] = 0; // Nothing in this tile
		}
	}
};

Grid.prototype.isPositionFree = function(row, col) {
	if (!this.grid[row] || this.grid[row][col] == null)
		return false;

	return !this.grid[row][col];
};

Grid.prototype.occupyPosition = function(row, col) {
	this.grid[row][col] = 1;
};

Grid.prototype.freePosition = function(row, col) {
	this.grid[row][col] = 0;
};

module.exports = Grid;