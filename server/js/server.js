// Load modules
var http = require('http').Server(),
	io = require('socket.io')(http),
	Match = require('./match'),
	Player = require('./player');

var Server = function() { };

/**
 * @static
 */
Server.start = function() {
	// Start socket server
	io.listen(8088);

	io.on('connection', function(socket) {
		var match;

		socket.on('disconnect', function() {
			if (match) {
				socket.broadcast.to(match.id).emit('opponent.disconnected');

				console.log('Broadcasted disconnection.');
			}

			console.log('Peer disconnected.');
		});

		socket.on('match.create', function() {
			// Create a new match
			var match = Match.create();

			// Emit event with new match id
			socket.emit('match.create', match.id);

			console.log('Match ' + match.id + ' created.');
		});

		socket.on('match.join', function(matchId, playerData) {
			// Find match
			match = Match.find(matchId);

			// Ensure match exists
			if (match) {
				// Instantiate a player object
				var player = new Player(playerData);

				// Store socket id to be used for communication
				player.setSocketId(socket.id);

				// Add player to match
				if (match.join(player)) {
					// Join socketio match room
					socket.join(matchId, function(err) {
						// Emit event to everyone in the match room
						socket.broadcast.to(matchId).emit('opponent.connected', player);

						// Notify sender
						socket.emit('match.join', matchId, player);

						// If there already is a player waiting
						if (match.players.length > 1) {
							// Send notify sender
							socket.emit('opponent.connected', match.players[0]);
						}
					});

					console.log('Player joined match ' + matchId + '.');
				} else {
					console.log('Match ' + matchId + ' is full.');
				}

			}
		});

		socket.on('player.ready', function(matchId, playerFbid, shipboardGrid) {
			// Find match
			match = Match.find(matchId);

			if (match) {
				// Set player grid and make it ready to begin match
				var player = match.findPlayer(playerFbid);
				player.setShipboardGrid(shipboardGrid);
				player.ready = true;

				// Broadcast event
				socket.broadcast.to(matchId).emit('opponent.ready');

				console.log('Player ready.');

				// Check if both players are ready
				if (match.canStart()) {
					// Emit event to everyone in the room
					io.sockets.to(matchId).emit('match.start');

					// First player to be ready starts
					var opponent = match.asPlayer(player.fbid).getOpponent();
					io.sockets.to(opponent.getSocketId()).emit('turn.start');

					// Notify player that his opponent's turn will start
					io.sockets.to(opponent.getSocketId()).emit('opponent.turn');

					console.log('Starting match.');
				}
			}
		});

		socket.on('player.fire', function(matchId, playerFbid, row, col) {
			match = Match.find(matchId);

			if (match) {
				// Get players
				var player = match.findPlayer(playerFbid);
				var opponent = match.asPlayer(player.fbid).getOpponent();

				// Shot on the opponent target
				var striked = opponent.fireAt(row, col);

				console.log('Player fired at ' + row + ':' + col);

				// Striked or missed?
				if (striked) {
					// Notify player he striked
					socket.emit('player.strike', row, col);

					// Notify opponent
					io.sockets.to(opponent.getSocketId()).emit('opponent.strike', row, col);

					console.log('Player striked at ' + row + ':' + col);

					// Check if opponent lost
					if (opponent.hasLost()) {
						// Notify player has won
						socket.emit('match.win');

						// Notify opponent has lost
						io.sockets.to(opponent.getSocketId()).emit('match.lose');

						// Notify everybody the match has ended
						io.sockets.to(matchId).emit('match.end');
					}
				} else {
					// Notify player he missed
					socket.emit('player.miss', row, col);
					socket.emit('turn.end');

					// Notify opponent
					io.sockets.to(opponent.getSocketId()).emit('opponent.miss', row, col);
					io.sockets.to(opponent.getSocketId()).emit('turn.start');

					console.log('Player missed at ' + row + ':' + col);
				}
			}
		});

		console.log('Peer connected.');
	});

	console.log('Server started.');
};

module.exports = Server;