// Load dependencies
var shortId = require('shortid');

// Store matches
var matches = [];

/**
 * Match object
 * @constructor
 * @param {String} id
 */
var Match = function(id) {
	this.id = id;
	this.players = [];
	this.ship_grid = [[]];
	this.combat_grid = [[]];
	this.turn = 0;
};

/**
 * @static
 * @return {Boolean}
 */
Match.create = function() {
	// Generate a new id
	var id = shortId.generate();

	// Create a new match instance
	var match = new Match(id);

	// Add object to matches list
	matches[id] = match;

	return match;
};

/**
 * @static
 * @param  {Number} matchId
 * @return {mixed}
 */
Match.find = function(matchId) {
	if (matches[matchId])
		return matches[matchId];

	return false;
};

/**
 * Join match
 * @param  {Object} playerData
 * @return {Boolean}
 */
Match.prototype.join = function(player) {
	if (this.players.length > 2)
		return false;

	// Add player to match
	this.players.push(player);

	return true;
};

/**
 * Leave match
 * @param  {Object} playerData
 */
Match.prototype.leave = function(playerData) {
	// Find player at this match
	var index = this.findPlayerIndex(playerData.fbid);

	// Ensure player was found
	if (index) {
		// Remove player from match
		this.players.slice(index, 1);
	}
};

/**
 * Find a player index in the match
 * @param  {Number} playerFbid Facebook ID
 * @return {Mixed}            Index if found or false if not found
 */
Match.prototype.findPlayerIndex = function(playerFbid) {
	for (var index in this.players) {
		if (playerFbid == this.players[index].fbid)
			return index;
	}

	return false;
};

/**
 * Find a player in the match
 * @param  {Number} playerFbid
 * @return {Player}
 */
Match.prototype.findPlayer = function(playerFbid) {
	// Search player index
	var index = this.findPlayerIndex(playerFbid);

	// Ensure player exists in this match
	if (index)
		return this.players[index];

	return false;
};

/**
 * Check if match can start
 * @return {Boolean} Returns true if both players are ready
 */
Match.prototype.canStart = function() {
	return (this.players.length == 2 && !!this.players[0].ready && !!this.players[1].ready);
};

/**
 * Make subsequent calls on current instance from a player perspective
 * @param  {Number} playerFbid
 * @return {Match} Match instance chainable
 */
Match.prototype.asPlayer = function(playerFbid) {
	var playerIndex = this.findPlayerIndex(playerFbid);

	if (!!playerIndex) {
		this.current_player = playerIndex;
		
		return this;
	}

	return null;
};

/**
 * Get opponent
 * @return {Player}
 */
Match.prototype.getOpponent = function() {
	// Make sure there is a current player set
	if (!!this.current_player) {
		var opponent = this.players[0];

		if (this.current_player == 0)
			opponent = this.players[1];

		return opponent;
	}
};

/**
 * Destroy match
 */
Match.prototype.destroy = function() {
	// Remove match from list
	for (var index in matches) {
		if (this.id == matches[index].id)
			matches.slice(index, 1);
	}
};

module.exports = Match;