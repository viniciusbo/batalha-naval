var Grid = require('./grid');

/**
 * @constructor
 * @param {Object} data
 */
var Player = function(data) {
	// Set player data
	this.fbid = data.fbid;
	this.name = data.name;
	this.pic_url = data.pic_url;
	this.ready = false;
	this.socket_id = null;

	// Initialize grids
	this.shipboard_grid = new Grid(10, 10);
	this.combatboard_grid = new Grid(10, 10);

	this.remaining_blocks = 0;
};

/**
 * Player is ready
 */
Player.prototype.ready = function() {
	this.ready = true;
};

/**
 * Set player socket id
 * @param {Socket} socketId
 */
Player.prototype.setSocketId = function(socketId) {
	this.socket_id = socketId;
};

/**
 * Return socket id
 * @return {SocketId} [description]
 */
Player.prototype.getSocketId = function() {
	return this.socket_id;
};

/**
 * Set shipboard grid
 * @param {Array} shipboardGrid
 */
Player.prototype.setShipboardGrid = function(shipboardGrid) {
	for (var row = 0; row < 10; row++) {
		// Ensure row exists
		if (shipboardGrid[row]) {
			for (var col = 0; col < 10; col++) {
				if (!!shipboardGrid[row][col]) {
					this.shipboard_grid.occupyPosition(row, col);

					this.remaining_blocks++;
					
					console.log('Shipboard grid pin at ' + row + ':' + col);
				}
			}
		}
	}
};

/**
 * Player was fired at position
 * @param  {Number} row
 * @param  {Number} col
 * @return {Boolean} True if striked or false if missed the shot
 */
Player.prototype.fireAt = function(row, col) {
	// Mark position as shot
	this.combatboard_grid.occupyPosition(row, col);

	// Check if shot was on target
	if (this.shipboard_grid.isPositionFree(row, col)) {
		// Missed
		return false;
	} else {
		// Striked
		this.remaining_blocks--;

		return true;
	}
};

/**
 * Check if player has lost
 * @return {Boolean}
 */
Player.prototype.hasLost = function() {
	return (this.remaining_blocks == 0);
}

module.exports = Player;