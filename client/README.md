# Battleship Client

## Server Object

### Events

- connect
	Triggered when successfuly connected to the server.
- connect_error
- disconnect
	Triggered when disconnected from the server.
- reconnect
	Triggered when reconnected to the server.
- message
	Triggered when a message is received.
- error
	Triggered when an error occoured on the server.

## Game Object

### Events

- ready
	Triggered when game is ready to begin.
- match.create
	Triggered when a new match is created.
- match.ready
	Triggered when the match is ready to be joined.
- match.start
	Triggered when the match has started.
- match.end
	Triggered when the match has ended.
- match.win
	Triggered when the player has won the match.
- match.lose
	Triggered when the player has lost the match.
- turn.start
	Triggered when the player's turn begins.
- turn.end
	Triggered when the player's turn ends.
- player.connected
	Triggered when the player is connected.
- player.fired
	Triggered when the player has "fired" on the board.
- opponent.ready
	Triggered when opponent has placed his ships on the board and is waiting for the match to begin.
- opponent.connected
	Triggered when the opponent is connected.
- opponent.fired
	Triggered when the opponent has "fired" on the board.

## Chat Object

- message.sent
- message.received