var Ships = (function(Types, Ship) {
	var Ships = {
		Patrol: Ship.extend({
			init: function(id) {
				this._super(id, Types.Ships.PATROL);
				this.size = 2;
			}
		}),

		Cruiser: Ship.extend({
			init: function(id) {
				this._super(id, Types.Ships.CRUISER);
				this.size = 3;
			}
		}),

		Submarine: Ship.extend({
			init: function(id) {
				this._super(id, Types.Ships.SUBMARINE);
				this.size = 3;
			}
		}),

		Battleship: Ship.extend({
			init: function(id) {
				this._super(id, Types.Ships.BATTLESHIP);
				this.size = 4;
			}
		}),

		Aircraft: Ship.extend({
			init: function(id) {
				this._super(id, Types.Ships.AIRCRAFT);
				this.size = 5;
			}
		})
	};

	return Ships;
})(Types, Ship);