var ShipBoard = (function($, Board) {
	var ShipBoard = Board.extend({
		ships: [],

		/**
		 * @constructor
		 * @param  {Object} settings Board settings
		 */
		init: function(settings) {
			this._super(settings);
		},

		/**
		 * Place a ship on the board
		 * @param {Ship} ship
		 */
		placeShip: function(ship) {
			var board = this;

			// Place ship on the board
			board.$el.append(ship.$el);

			// Add ship to list
			board.ships.push(ship);

			// Initialize drag behavior
			ship.iel
			    .draggable(true)
			    .origin({ x: 0, y: 0 })
			    .snap({
			        mode: 'grid',
			        grid: { x: board.grid_size, y: board.grid_size },
			        gridOffset: { x: 1, y: 1 }
			    })
			    .on('dragmove', function (event) {
			    	// Store position delta
			    	ship.dx += event.dx;
			    	ship.dy += event.dy;

			    	// Grid drag
			    	var dx = Math.round(ship.dx / board.grid_size) * board.grid_size;
			    	var dy = Math.round(ship.dy / board.grid_size) * board.grid_size;

		    		if (Math.abs(dx) == board.grid_size)
		    	        ship.dx = 0;

		    	    if (Math.abs(dy) == board.grid_size)
		    	        ship.dy = 0;
			    	// Calculate position
			    	var x = ship.x + dx;
			    	var y = ship.y + dy;

			    	// Check if position has changed
			    	if (x != ship.x || y != ship.y) {
				    	// Restrict movements on the board edges
				    	if (x < 0
				    		|| y < 0
				    		|| x + ship.$el.width() > board.width
				    		|| y + ship.$el.height() > board.height)
				    		return;

				    	// Get current grid position
				    	var gridPos = board.getGridPosition(x, y);

				    	// Free current ship area
				    	board.grid.freeLinearArea(ship.grid.row, ship.grid.col, ship.size - 1, ship.orientation)

			    		// Check if destination area is available
			    		if (!board.grid.isLinearAreaFree(gridPos.row, gridPos.col, ship.size - 1, ship.orientation)) {
			    			board.grid.occupyLinearArea(ship.grid.row, ship.grid.col, ship.size - 1, ship.orientation)
			    			return;
			    		}

				    	// Set ship position on the board
				    	board.setShipPosition(ship, x, y, gridPos.row, gridPos.col);
			    	}
			    })
				.on('dragend', function(event) {
			    	// Erase position delta
			    	ship.dx = 0;
			    	ship.dy = 0;
				});

			// Change ship orientation
			ship.$el.on('dblclick', document, function(event) {
				event.preventDefault();

				var inverseOrientation, destRow, destCol;

				if (ship.orientation == 'horizontal') {
					inverseOrientation = 'vertical';
					
					destRow = ship.grid.row + ship.size - 1;
					destCol = ship.grid.col;
				} else {
					inverseOrientation = 'horizontal';

					destRow = ship.grid.row;
					destCol = ship.grid.col + ship.size - 1;
				}

		    	// Free current ship area
		    	board.grid.freeLinearArea(ship.grid.row, ship.grid.col, ship.size - 1, ship.orientation)

				// Check if there are enough available space to flip
				if (board.grid.isLinearAreaFree(ship.grid.row, ship.grid.col, ship.size - 1, inverseOrientation)) {
					// Toggle ship orientation
					ship.toggleOrientation();

					// Refresh grid
					board.refreshGrid();
				} else {
					// Restore freed area
					board.grid.occupyLinearArea(ship.grid.row, ship.grid.col, ship.size - 1, ship.orientation)
				}
			});

			// Set ship dimensions based on grid size
			ship.setDimensions(board.grid_size);
		},

		/**
		 * Set ship position on the board
		 * @param {Ship} ship
		 */
		setShipPosition: function(ship, x, y, row, col) {
			// Update ship position
			ship.setPosition(x, y);
			ship.setGridPosition(row, col);

			// Refresh grid with updated positions
			this.refreshGrid();

			ship.el.style.top = ship.y + 'px';
			ship.el.style.left = ship.x + 'px';
		},

		/**
		 * Refresh grid
		 */
		refreshGrid: function() {
			var board = this;

			// Clear grid
			board.grid.clearGrid();

			// Set every ship position on grid
			board.ships.forEach(function(ship) {
				if (ship.orientation == 'horizontal') {
					for (var col = ship.grid.col; col < ship.grid.col + ship.size; col++)
						board.grid.occupyPosition(ship.grid.row, col);
				} else {
					for (var row = ship.grid.row; row < ship.grid.row + ship.size; row++)
						board.grid.occupyPosition(row, ship.grid.col);
				}
			});
		},

		pinShips: function() {
			// Set every ship position on grid
			this.ships.forEach(function(ship) {
				ship.iel.unset();
				ship.$el.off('dblclick');
			});
		}
	});

	return ShipBoard;
})(jQuery, Board);