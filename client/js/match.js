var Match = (function() {
	var Match = Class.extend({
		id: null,
		player: null,
		opponent: null,

		/**
		 * @constructor
		 */
		init: function() {

		},

		/**
		 * Set current player
		 * @param {Player} player
		 */
		setPlayer: function(player) {
			this.player = player;
		},

		/**
		 * Get player
		 * @return {Player}
		 */
		getPlayer: function() {
			return this.player;
		},

		/**
		 * Set opponent
		 * @param {Player} opponent
		 */
		setOpponent: function(opponent) {
			this.opponent = opponent;
		},

		/**
		 * Join match with specified id
		 * @param  {Number} matchId
		 */
		join: function(matchId) {
			this.id = matchId;
		}
	});

	return Match;
})();