var Server = (function(_, io, EventEmitter) {
	var Server = Class.extend({
		socket: null, // SocketIo Instance
		room: null, // Current SocketIo room

		/**
		 * @constructor
		 * @param  {Object} config Server configuration
		 */
		init: function(config) {
			// Connect
			this.socket = io('http://' + config.host + ':' + config.port);

			// Event handlers
			this.socket.on('connect', this._connectHandler.bind(this));
			this.socket.on('connect_error', this._connectErrorHandler.bind(this));
			this.socket.on('error', this._connectErrorHandler.bind(this));
			this.socket.on('message', this._messageHandler.bind(this));
			this.socket.on('disconnect', this._disconnectHandler.bind(this));
			this.socket.on('reconnect', this._reconnectHandler.bind(this));
		},

		/**
		 * Connection handler
		 * @return {Socket} SocketIo Socket obj
		 */
		_connectHandler: function(socket) {
			this.emit('connect', socket);
		},

		_connectErrorHandler: function(error) {
			this.emit('connect_error', error);
		},

		/**
		 * Error handler
		 */
		_errorHandler: function(error) {
			this.emit('error', error);
		},

		_messageHandler: function(data) {
			this.emit('message', data);
		},

		_disconnectHandler: function() {
			this.emit('disconnect');
		},

		_reconnectHandler: function() {
			this.emit('reconnect');
		},

		/**
		 * Return socket object
		 * @return {Socket} SocketIo Socket obj
		 */
		getSocket: function() {
			return this.socket;
		}
	});
	
	// "Inheritance" from EventEmitter
	_.assign(Server.prototype, EventEmitter.prototype);

	return Server;
})(_, io, EventEmitter);