var Ship = (function(Piece, interact) {
	var Ship = Piece.extend({
		id: null,
		type: null,
		iel: null,

		// Horizontal or vertical
		orientation: 'horizontal',

		// Qty of tiles to occupy
		size: null,

		/**
		 * @constructor
		 * @param  {Number} id
		 * @param  {Type} type
		 */
		init: function(id, type) {
			this._super();

			// Set interact element
			this.iel = interact(this.el);

			this.id = id;
			this.type = type;
		},

		/**
		 * Set dimensions
		 * @param {Number} gridSize
		 */
		setDimensions: function(gridSize) {
			var width = (this.size * gridSize) - 2;
			var height = gridSize - 2;

			this.$el.css({
				width: width,
				height: height
			});
		},

		/**
		 * Toggle orientation (horizontal/vertical)
		 */
		toggleOrientation: function() {
			if (this.orientation == 'horizontal') {
				this.orientation = 'vertical';
			} else {
				this.orientation = 'horizontal';
			}

			var currentW = this.el.style.width;

			this.el.style.width = this.el.style.height;
			this.el.style.height = currentW;
		}
	});

	return Ship;
})(Piece, interact);