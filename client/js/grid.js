var Grid = (function() {
	var Grid = Class.extend({
		rows: 0,
		cols: 0,
		grid: [[]],
		occupied_count: 0,

		/**
		 * Initialize grid
		 * @param  {Object} settings
		 */
		init: function(settings) {
			this.rows = settings.rows;
			this.cols = settings.cols;

			// Start clear grid
			this.clearGrid();
		},

		/**
		 * Clear all grid positions
		 */
		clearGrid: function() {
			for (var row = 0; row < this.rows; row++) {
				this.grid[row] = [];

				for (var col = 0; col < this.cols; col++) {
					this.grid[row][col] = 0; // Nothing in this tile
				}
			}

			this.occupied_count = 0;
		},

		/**
		 * Check if a given linear area is free
		 * @param  {Number}  startRow
		 * @param  {Number}  startCol
		 * @param  {Number}  length
		 * @param  {String}  orientation
		 * @return {Boolean}
		 */
		isLinearAreaFree: function(startRow, startCol, length, orientation) {
			var endRow, endCol;

			// this.freeArea(currentRow, currentCol, length, currentOrientation);

			// Set start and end tiles
			if (orientation == 'horizontal') {
				endRow = startRow;
				endCol = startCol + length;
			} else {
				endRow = startRow + length;
				endCol = startCol;
			}

			// Check if they are free
			for (var row = startRow; row <= endRow; row++) {
				for (var col = startCol; col <= endCol; col++) {
					// Return false when first filled position found
					if (!this.isPositionFree(row, col))
						return false;
				}
			}

			return true;
		},

		/**
		 * Check if a given position is free to move
		 * @param  {Number}  row
		 * @param  {Number}  col
		 * @return {Boolean}
		 */
		isPositionFree: function(row, col) {
			if (!this.grid[row] || this.grid[row][col] == null)
				return false;

			return !this.grid[row][col];
		},

		/**
		 * Occupy a position in grid
		 * @param  {Number} row
		 * @param  {Number} col
		 */
		occupyPosition: function(row, col) {
			this.grid[row][col] = 1;

			this.occupied_count++;
		},

		/**
		 * Occupy a linear area in grid
		 * @param  {Number} startRow
		 * @param  {Number} startCol
		 * @param  {Number} length
		 * @param  {String} orientation
		 */
		occupyLinearArea: function(startRow, startCol, length, orientation) {
			var endRow, endCol;

			// Set start and end tiles
			if (orientation == 'horizontal') {
				endRow = startRow;
				endCol = startCol + length;
			} else {
				endRow = startRow + length;
				endCol = startCol;
			}

			// Check if they are free
			for (var row = startRow; row <= endRow; row++) {
				for (var col = startCol; col <= endCol; col++) {
					this.occupyPosition(row, col);
				}
			}
		},

		/**
		 * Free a specific position in grid
		 * @param  {Number} row
		 * @param  {Number} col
		 */
		freePosition: function(row, col) {
			this.grid[row][col] = 0;

			this.occupied_count--;
		},

		/**
		 * Free a linear area in grid
		 * @param  {Number} startRow
		 * @param  {Number} startCol
		 * @param  {Number} length
		 * @param  {String} orientation
		 */
		freeLinearArea: function(startRow, startCol, length, orientation) {
			var endRow, endCol;

			// Set start and end tiles
			if (orientation == 'horizontal') {
				endRow = startRow;
				endCol = startCol + length;
			} else {
				endRow = startRow + length;
				endCol = startCol;
			}

			// Check if they are free
			for (var row = startRow; row <= endRow; row++) {
				for (var col = startCol; col <= endCol; col++) {
					this.freePosition(row, col);
				}
			}
		}
	});

	return Grid;
})();