var Player = (function() {
	var Player = Class.extend({
		fbid: null,
		name: null,
		pic_url: null,

		/**
		 * @constructor
		 * @param  {Object} data Player data
		 */
		init: function(data) {
			this.fbid = data.fbid;
			this.name = data.name;
			this.pic_url = data.pic_url;
		},

		/**
		 * Get name
		 * @return {String} Player name
		 */
		getName: function() {
			return this.name;
		},

		/**
		 * Get pic url
		 * @return {String} Picture URL
		 */
		getPicUrl: function() {
			return this.pic_url;
		}
	});

	return Player;
})();