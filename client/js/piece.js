var Piece = (function($) {
	var Piece = Class.extend({
		el: null, // dom element
		$el: null, // jquery element

		// Position on board
		x: 0,
		y: 0,

		// Position on grid
		grid: {
			row: 0,
			col: 0
		},

		// Position delta
		dx: 0,
		dy: 0,

		// Ship template
		tpl: '<div class="shipboard-ship gameboard-piece"></div>',

		/**
		 * @constructor
		 */
		init: function() {
			this.$el = $(this.tpl);
			this.el = this.$el.get(0);
		},

		/**
		 * Set current position
		 * @param {Number} x
		 * @param {Number} y
		 */
		setPosition: function(x, y) {
			this.x = x;
			this.y = y;
		},

		/**
		 * Set current grid position
		 * @param {Number} row
		 * @param {Number} col
		 */
		setGridPosition: function(row, col) {
			this.grid = {
				row: row,
				col: col,
			};
		}
	});

	return Piece;
})($, interact);