(function($, Router, Game, Server, Player) {
	var game = new Game();
	var router;
	var matchId;

	/**
	 * Initialize server object and its event handlers
	 * @return {Server}
	 */
	var initServer = function() {
		// Connect to server
		var server = new Server({
			host: 'localhost',
			port: 8088
		});

		// Server event handling
		server.on('connect', function() {
			console.log('Conectado ao servidor.');
		});

		// Server event handling
		server.on('connect_error', function() {
			console.log('Erro ao conectar ao servidor.');
		});

		server.on('error', function() {
			console.log('Erro no servidor.');
		});

		server.on('disconnect', function() {
			console.log('Desconectado do servidor.');
		});

		return server;
	};

	// Game initializator
	var initGame = function() {
		// Initialize server
		var server = initServer();

		// Create a new game instance
		game.setServer(server);

		game.on('ready', function() {
			console.log('Game ready.');

			// Change button text
			$('#play').text('Entrando na partida...');

			// Create a new match or join existing one
			if (!!matchId)
				game.joinMatch(matchId);
			else
				game.createMatch();
		});

		game.on('player.connected', function(player) {
			// Join current match
			game.setPlayer(player);

			// Update UI with player info
			$('#player-pic').prop('src', player.getPicUrl());
			$('#player-name').text(player.getName());

			// Emit game ready event
			game.emit('ready');
		});

		game.on('match.ready', function(matchId) {
			$('#play').closest('.game-view').addClass('transition-perspectiveUp');

			// Set current route
			router.setRoute('/m/' + matchId);
		});

		game.on('match.start', function() {
			console.log('A partida começou.');

			$('#ready').remove();
		});

		game.on('match.end', function() {
			console.log('A partida terminou.');

			router.setRoute('/');

			window.reload();
		});

		game.on('match.win', function() {
			console.log('Você venceu a partida.');

			alert('Você venceu a partida!');
		});

		game.on('match.lose', function() {
			console.log('Você perdeu a partida.');

			alert('Você perdeu a partida...');
		});

		game.on('opponent.connected', function(opponent) {
			console.log('Um oponente entrou na partida.');

			var opponent = new Player(opponent);

			// Update UI with player info
			$('#opponent-pic').prop('src', opponent.getPicUrl());
			$('#opponent-name').text(opponent.getName());

			// Set current match opponent
			game.match.setOpponent(opponent);

			// Enable ready btn
			$('#ready').prop('disabled', false);
		});

		game.on('opponent.ready', function(opponent) {
			console.log('O oponente está pronto.');
		});

		game.on('opponent.disconnected', function() {
			// Update UI
			$('#opponent-pic').prop('src', '');
			$('#opponent-name').text('Aguardando oponente...');

			// Clear match opponent
			game.match.setOpponent(null);

			router.setRoute('/');

			console.log('O oponente saiu.');
		});

		game.on('turn.start', function() {
			console.log('O seu turno começou.');

			$('.game-playarea').removeClass('transition-slideDown').addClass('transition-slideUp');
		});

		game.on('turn.end', function() {
			console.log('O seu turno terminou.');

			$('.game-playarea').removeClass('transition-slideUp').addClass('transition-slideDown');
		});

		game.on('player.strike', function(row, col) {
			$('#cb-u-' + row + '-' + col).addClass('hit hit-strike').off('click');
			
			console.log('Você acertou o tiro em ' + row + ':' + col + '.');
		});

		game.on('player.miss', function(row, col) {
			$('#cb-u-' + row + '-' + col).addClass('hit hit-miss').off('click');

			console.log('Você errou o tiro em ' + row + ':' + col + '.');
		});

		game.on('opponent.strike', function(row, col) {
			console.log('O oponente acertou o tiro em ' + row + ':' + col + '.');
		});

		game.on('opponent.miss', function(row, col) {
			console.log('O oponente errou o tiro em ' + row + ':' + col + '.');
		});

		$(document).ready(function() {
			$('#ready').click(function(e) {
				e.preventDefault();

				$(this).text('Aguardando oponente...').prop('disabled', true);

				game.playerReady();
			});

			$('.combatboard-unit').click(function(e) {
				e.preventDefault();

				var row = $(this).data('row');
				var col = $(this).data('col');

				game.fireAt(row, col);

				console.log('Atirou em ' + row + ':' + col);
			})
		});
	};

	// Initiailize routing
	var initRouting = function() {
		var routes = {
			'/': function() {
				matchId = null;

				$('#play').closest('.game-view').removeClass('transition-perspectiveUp').addClass('transition-perspectiveDown');
			},
			'/m/:id': function(id) {
				// Set global game id
				matchId = id;

				// Change button text based on current context
				$('#play').text('Entrar na partida');
			}
		};

		// Create a DirectorJs instance
		router = Router(routes);

		// Initialize router
		router.init('/');
	};

	var loginWithFacebook = function(fbResponse) {
		var player = new Player({
			fbid: fbResponse.id,
			name: fbResponse.first_name,
			pic_url: '//graph.facebook.com/' + fbResponse.id + '/picture?type=large'
		});

		// Trigger event
		game.emit('player.connected', player);
	};

	// App initializator
	var initApp = function() {
		initGame();

		$(document).ready(function() {
			initRouting();

			$('#play').click(function(e) {
				e.preventDefault();

				// Change button text
				$(this).text('Entrando...');

				var FB = window.FB;

				FB.getLoginStatus(function(response) {
					if (response.status == 'connected') {
						FB.api('/me', loginWithFacebook);
					} else {
						FB.login(function(response) {
							if (response.authResponse) {
								FB.api('/me', loginWithFacebook);
							} else {
								console.log('User cancelled login or did not fully authorize.');
							}
						});
					}
				});
			});
		});

		window.fbAsyncInit = function() {
			FB.init({
				appId: '284331201738028',
				status: true,
				xfbml: true,
				version: 'v2.0'
			});

			// Enable play btn
			var play = document.getElementById('play');
			play.disabled = false;
		};
	};

	initApp();
})(jQuery, Router, Game, Server, Player);