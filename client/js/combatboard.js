var CombatBoard = (function(Board) {
	var CombatBoard = Board.extend({
		/**
		 * @constructor
		 * @param  {Object} settings Board settings
		 */
		init: function(settings) {
			this._super(settings);

			this._initBlocks();
		},

		_initBlocks: function() {
			for (var row = 0; row < 10; row++) {
				for (var col = 0; col < 10; col++) {
					this.$el.append('<div class="combatboard-unit" data-row="' + row + '" data-col="' + col + '" id="cb-u-' + row + '-' + col + '">');
				}
			}
		}
	});

	return CombatBoard;
})(Board);