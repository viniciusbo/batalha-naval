var Board = (function(Grid) {
	var Board = Class.extend({
		grid_size: null,
		width: null,
		height: null,

		$el: null, // jquery object

		grid: null, // grid object
		pieces: [],

		/**
		 * @constructor
		 * @param  {Object} settings
		 */
		init: function(settings) {
			var self = this;

			// Ensure an DOM element was given
			if (!settings.board)
				console.error('No board selector specified.');

			// Cache DOM element
			var el = document.querySelector(settings.board);

			// Set object jQuery element
			self.$el = $(el);

			// Set grid size
			self.grid_size = settings.grid_size;

			// Instantiate a new grid
			self.grid = new Grid({
				rows: settings.rows,
				cols: settings.cols
			});

			// Set board size
			self.width = self.grid.cols * self.grid_size;
			self.height = self.grid.rows * self.grid_size;

			// Initialize board CSS
			// self._initBoardStyles();
		},

		/**
		 * Initialize board CSS
		 */
		_initBoardStyles: function() {
			// Set board dimensions
			// this.$el.css({
			// 	width: this.width + 'px',
			// 	height: this.height + 'px'
			// });
		},

		/**
		 * Get grid position based on x and y
		 * @param  {Number} x
		 * @param  {Number} y
		 * @return {Object}
		 */
		getGridPosition: function(x, y) {
			var pos = {};

			pos.row = Math.round(y / this.grid_size);
			pos.col = Math.round(x / this.grid_size);

			return pos;
		},
	});

	return Board;
})(Grid);