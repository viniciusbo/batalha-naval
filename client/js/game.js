var Game = (function(_, EventEmitter, Match, ShipBoard, CombatBoard, Ships) {
	var Game = Class.extend({
		server: null, // Server instance
		match: null,

		ship_board: null,
		combat_board: null,
		ships: {},

		/**
		 * @constructor
		 * @param  {Server} server
		 */
		init: function() {
			this.ship_board = new ShipBoard({
				board: '#battleship-board',
				grid_size: 60,
				rows: 10,
				cols: 10
			});
			this.shot_board = new CombatBoard({
				board: '#battleship-combatboard',
				grid_size: 60,
				rows: 10,
				cols: 10
			});

			this._createShips();
			this._placeShipsOnBoard();
			this._initMatch();
		},

		/**
		 * Create ships
		 */
		_createShips: function() {
			// Create ships
			this.ships['patrol'] = new Ships.Patrol(1);
			this.ships['cruiser'] = new Ships.Cruiser(2);
			this.ships['submarine'] = new Ships.Submarine(3);
			this.ships['battleship'] = new Ships.Battleship(4);
			this.ships['aircraft'] = new Ships.Aircraft(5);
		},

		/**
		 * Place ships on the board
		 */
		_placeShipsOnBoard: function() {
			// Place all ships on the board
			for (var shipIndex in this.ships) {
				this.ship_board.placeShip(this.ships[shipIndex]);
			}
		},

		/**
		 * Initialize match
		 */
		_initMatch: function() {
			this.match = new Match();
		},

		/**
		 * Initialize match events
		 */
		_initMatchEvents: function() {
			var self = this;

			self.server.socket.on('opponent.connected', function(opponent) {
				self.emit('opponent.connected', opponent);
			});

			self.server.socket.on('opponent.disconnected', function() {
				self.emit('opponent.disconnected');
			});
		},

		/**
		 * Set server instance
		 * @param {Server} server
		 */
		setServer: function(server) {
			this.server = server;
		},

		/**
		 * Set player
		 * @param {Player} player
		 */
		setPlayer: function(player) {
			this.match.setPlayer(player);
		},

		/**
		 * Create a new match
		 */
		createMatch: function() {
			var self = this;

			// Request a new match to the server
			self.server.socket.emit('match.create');

			// Join match when server responds
			self.server.socket.on('match.create', function(matchId) {
				self.joinMatch(matchId);

				console.log('Partica criada.');
			});
		},

		/**
		 * Join an existing match
		 * @param  {Number} matchId
		 */
		joinMatch: function(matchId) {
			var self = this;

			// Notify server the user wants to join this match
			self.server.socket.emit('match.join', matchId, self.match.getPlayer());

			// When user joins a match
			self.server.socket.on('match.join', function(matchId, player) {
				if (matchId) {
					console.log('Entrou na partida.');

					// Join an existing match
					self.match.join(matchId);

					// Emit event
					self.emit('match.ready', matchId);

					// Initialize match events
					self._initMatchEvents();
				} else {
					console.log('Não foi possível entrar na partida "' + matchId + '".');
				}
			});

			// When the match starts
			self.server.socket.on('match.start', function() {
				self.emit('match.start');
			});

			// When the match ends
			self.server.socket.on('match.end', function() {
				self.emit('match.end');
			});

			// When the player wins the match
			self.server.socket.on('match.win', function() {
				self.emit('match.win');
			});

			// When the player looses the match
			self.server.socket.on('match.lose', function() {
				self.emit('match.lose');
			});

			// When opponent is ready
			self.server.socket.on('opponent.ready', function() {
				self.emit('opponent.ready');
			});

			// When turn starts
			self.server.socket.on('turn.start', function() {
				self.emit('turn.start');
			});

			// When turn ends
			self.server.socket.on('turn.end', function() {
				self.emit('turn.end');
			});

			// When player fire hits on target
			self.server.socket.on('player.strike', function(row, col) {
				self.emit('player.strike', row, col);
			});

			// When player fire is miss
			self.server.socket.on('player.miss', function(row, col) {
				self.emit('player.miss', row, col);
			});

			// When opponent fire hits on target
			self.server.socket.on('opponent.strike', function(row, col) {
				self.emit('opponent.strike', row, col);
			});

			// When opponent fire is miss
			self.server.socket.on('opponent.miss', function(row, col) {
				self.emit('opponent.miss', row, col);
			});

			// When opponent turn starts
			self.server.socket.on('opponent.turn', function() {
				self.emit('opponent.turn');
			});
		},

		/**
		 * Player is ready
		 */
		playerReady: function() {
			// Don't let ships move anymore
			this.ship_board.pinShips();

			// Emit event
			this.server.socket.emit('player.ready', this.match.id, this.match.player.fbid, this.ship_board.grid.grid);
		},

		fireAt: function(row, col) {
			this.server.socket.emit('player.fire', this.match.id, this.match.player.fbid, row, col);
		}
	});
	
	// Inherit from EventEmitter
	_.assign(Game.prototype, EventEmitter.prototype)

	return Game;
})(_, EventEmitter, Match, ShipBoard, CombatBoard, Ships);