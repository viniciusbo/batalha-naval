/**
 * Define shared game types
 */
var Types = {
	Ships: {
		PATROL: 1,
		CRUISER: 2,
		SUBMARINE: 3,
		BATTLESHIP: 4,
		AIRCRAFT: 5
	}
}